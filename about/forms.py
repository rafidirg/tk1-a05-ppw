from django import forms
from .models import Feedback

class FeedbackForm(forms.ModelForm):
    
    class Meta:
        model = Feedback
        fields = ('feedback',)
        labels = {
            'feedback' : ''
        }
        widgets = {
            'feedback' : forms.Textarea(attrs={
                'placeholder': ' I feel ...',
                'style': 'width: 100% ; background-color:#F7FBE1; border:0; font-family: "Poppins Light"; padding:5px; ',
                'cols':5, 'rows':5,
                },)
        }
