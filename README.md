# tkppwa05

![pipeline status](https://gitlab.com/RiDirg/tk1-a05-ppw/badges/master/pipeline.svg)
![coverage report](https://gitlab.com/RiDirg/tk1-a05-ppw/badges/master/coverage.svg)

## Daftar isi

- [Daftar isi](#daftar-isi)
- [Nama Anggota Kelompok](#nama-anggota-kelompok)
- [Link Heroku](#link-heroku)
- [Cerita Aplikasi](#cerita-aplikasi)
- [Fitur yang diimplementasikan](#fitur-yang-diimplementasikan)

## Nama Anggota Kelompok

1. Ahmad Imam Fauzan - 1906353542
2. Rafi Indrawan Dirgantara - 1906350824
3. Rasendrio Muhammad - 1906303084
4. Zahrah Rahmani Putri - 1906351032

## Link Heroku

my-footprints.herokuapp.com

## Cerita Aplikasi

Aplikasi ini ditujukan untuk mengetahui jejak kemana saja ketika anda
keluar rumah. Apabila anda terkena virus COVID, anda dapat mengetahui
tempat mana saja yang anda sudah kunjungi sebelumnya sehingga anda
dapat memberitahukan bahwa tempat tersebut terdapat virus COVID.

## Fitur yang diimplementasikan

1. Login page
2. Landing page
3. Form input kapan keluar rumah
4. Form input feedback mengenai aplikasi ini
5. Form input perasaan user mengenai pandemi
6. Profile page