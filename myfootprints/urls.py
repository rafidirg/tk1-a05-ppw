from django.urls import path

from . import views

app_name = 'myfootprints'

urlpatterns = [
    path('my-footprints/', views.myfootprints, name='myfootprints'),
    path('add-place/', views. addplace, name='addfootprints'),
    path('delete-place/<int:id>', views.delete, name="delete-place"),
    path('update-place/<int:id>', views.update, name='update-place'),
]
