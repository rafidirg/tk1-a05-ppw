from django.shortcuts import render, redirect
from .models import MyFootPrints
from .forms import MyFootPrintsForm
import datetime

# Create your views here.

def myfootprints(request):
    places = MyFootPrints.objects.filter(user=request.user)
    return render(request, 'myfootprints.html', {'places':places})

def addplace(request):
    form = MyFootPrintsForm()
    if request.method == 'POST':
        form = MyFootPrintsForm(request.POST)
        place = request.POST['place']
        date_time_str = request.POST['date']
        desc = request.POST['desc']
        if form.is_valid():
            user = request.user
            date_time_obj = datetime.datetime.strptime(date_time_str, '%m/%d/%Y')
            fp = MyFootPrints(place=place, date=date_time_obj, desc=desc, user=user)
            fp.save()
            # form.save()
            return redirect('/my-footprints')
    return render(request, 'addplace.html', {'form':form})

def update(request, id):
    place = MyFootPrints.objects.get(id=id)
    form = MyFootPrintsForm(instance=place)
    if request.method == 'POST':
        form = MyFootPrintsForm(request.POST, instance=place)
        if form.is_valid():
            form.save()
            return redirect('/my-footprints')
    return render(request, 'addplace.html', {'form':form})

def delete(request, id):
    place = MyFootPrints.objects.get(id=id)
    place.delete()
    return redirect('/my-footprints')