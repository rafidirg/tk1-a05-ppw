from django.test import TestCase, Client
from .models import UserProfile
from django.contrib.auth.models import User

SIGN_IN_URL = '/auth/signin/'
SIGN_UP_URL = '/auth/signup/'
PROFILE_URL = '/auth/profile/'

class TestAuthApp(TestCase):
    def setUp(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        user.save()
        prof = UserProfile.objects.create(user = user, full_name='John Lennon')
        prof.save()

    def test_signup_url_exists(self):
        response = self.client.get(SIGN_UP_URL)
        self.assertEquals(response.status_code, 200)
    
    def test_template_used_on_signup(self):
        response = self.client.get(SIGN_UP_URL)
        self.assertTemplateUsed(response, 'authapp/signup.html')

    def test_create_new_user_from_url(self):
        total_user = User.objects.all().count()
        total_profile = UserProfile.objects.all().count()
        passed_data = {
            'username':'thisisusername',
            'password1':'ThisIsAP@ssword123',
            'password2':'ThisIsAP@ssword123',
            'email':'thisisemail@mail.org',
            'full_name':'my full name',
        }
        response = self.client.post(SIGN_UP_URL, data = passed_data)
        self.assertEqual(response.status_code, 302)
        self.assertEquals(User.objects.all().count(), total_user+1)
        self.assertEquals(UserProfile.objects.all().count(), total_profile+1)
    
    def test_create_new_user(self):
        total_user = User.objects.all().count()
        total_profile = UserProfile.objects.all().count()
        user = User.objects.create_user('duar', 'duar@mail.com', '54321raud')
        user.save()
        prof = UserProfile.objects.create(user = user, full_name='DUARRRR')
        prof.save()
        self.assertEquals(str(prof), 'duar')
        self.assertEquals(User.objects.all().count(), total_user+1)
        self.assertEquals(UserProfile.objects.all().count(), total_profile+1)
    
    def test_signin_url_exists(self):
        response = self.client.get(SIGN_IN_URL)
        self.assertEquals(response.status_code, 200)

    def test_template_used_on_signin(self):
        response = self.client.get(SIGN_IN_URL)
        self.assertTemplateUsed(response, 'authapp/signin.html')

    def test_success_signin_john(self):
        passed_data = {
            'username':'john',
            'password':'johnpassword'
        }
        response = self.client.post(SIGN_IN_URL, data = passed_data)
        content_response = response.content.decode('utf8')
        self.assertEquals(response.status_code, 302)
        self.assertIn(content_response, 'Success')

    def test_failed_signin_john(self):
        passed_data = {
            'username':'john',
            'password':'johnpasswo'
        }
        response = self.client.post(SIGN_IN_URL, data =  passed_data)
        content_response = response.content.decode('utf8')
        self.assertEquals(response.status_code, 200)
        self.assertIn('Username or Password is Incorrect', content_response)
    
    def test_profile_url_exists(self):
        response = self.client.get(PROFILE_URL)
        self.assertEquals(response.status_code, 200)
           
    def test_profile_signined_user(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get(PROFILE_URL)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'authapp/profile.html')
        content_response = response.content.decode('utf8')
        self.assertIn('John Lennon', content_response)
        self.assertIn('john', content_response)
        self.assertIn('lennon@thebeatles.com', content_response)
    
    def test_profile_unsignined_user(self):
        response = self.client.get(PROFILE_URL)
        self.assertEqual(response.status_code, 200)
        content_response = response.content.decode('utf8')
        self.assertIn('Please Login First', content_response)
    
    def test_logout_user(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/auth/logout/')
        self.assertEquals(response.status_code, 200)
