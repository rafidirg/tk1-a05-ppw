from django.test import Client, TestCase
from django.contrib.auth.models import User
from .models import Feeling
from .forms import FeelingForm
from authapp.models import UserProfile

# Create your tests here.
class Testing123(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test',password='passs2020')    

    def test_apakah_url_info_corona_ada(self):
        response = Client().get('/info-corona/')
        self.assertEquals(response.status_code, 200)
    
    def test_apakah_di_halaman_info_corona_ada_templatenya(self):
        response = Client().get('/info-corona/')
        self.assertTemplateUsed(response, 'info-corona.html')

    def test_apakah_sudah_ada_model_feeling(self):
        u = UserProfile.objects.create(user=self.user, full_name='test past')
        Feeling.objects.create(user=u, feeling='my feeling this year')
        hitung = Feeling.objects.all().count()
        self.assertEquals(hitung, 1)
    
    def test_apakah_dihalaman_info_corona_ada_text_judul(self):
        respons = Client().get('/info-corona/')
        html_kembalian = respons.content.decode('utf8')
        self.assertIn("WHAT IS CORONA VIRUS?", html_kembalian)

